package com.saucedemo.pageObject;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class ProductosPageObject extends PageObject {
    By btnAñadirAlCarrito = By.xpath("//button[@id='add-to-cart-sauce-labs-backpack']");
    By btnRemoverCarrito = By.xpath("//button[@id='remove-sauce-labs-backpack']");
    By imgProducto = By.xpath("//body/div[@id='root']/div[@id='page_wrapper']/div[@id='contents_wrapper']/div[@id='inventory_container']/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]/img[1]");
    By btnVolverInicio = By.xpath("//button[@id='back-to-products']");
    By btnCarrito = By.xpath("//body/div[@id='root']/div[@id='page_wrapper']/div[@id='contents_wrapper']/div[@id='header_container']/div[1]/div[3]/a[1]");
    By btnComprar = By.xpath("//button[@id='checkout']");

    public By getBtnAñadirAlCarrito() {
        return btnAñadirAlCarrito;
    }

    public void setBtnAñadirAlCarrito(By btnAñadirAlCarrito) {
        this.btnAñadirAlCarrito = btnAñadirAlCarrito;
    }

    public By getBtnRemoverCarrito() {
        return btnRemoverCarrito;
    }

    public void setBtnRemoverCarrito(By btnRemoverCarrito) {
        this.btnRemoverCarrito = btnRemoverCarrito;
    }

    public By getImgProducto() {
        return imgProducto;
    }

    public void setImgProducto(By imgProducto) {
        this.imgProducto = imgProducto;
    }

    public By getBtnVolverInicio() {
        return btnVolverInicio;
    }

    public void setBtnVolverInicio(By btnVolverInicio) {
        this.btnVolverInicio = btnVolverInicio;
    }

    public By getBtnCarrito() {
        return btnCarrito;
    }

    public void setBtnCarrito(By btnCarrito) {
        this.btnCarrito = btnCarrito;
    }

    public By getBtnComprar() {
        return btnComprar;
    }

    public void setBtnComprar(By btnComprar) {
        this.btnComprar = btnComprar;
    }
}
