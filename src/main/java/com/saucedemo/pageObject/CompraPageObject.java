package com.saucedemo.pageObject;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class CompraPageObject extends PageObject {
    By inputNombre = By.xpath("//input[@id='first-name']");
    By inputApellido = By.xpath("//input[@id='last-name']");
    By inputCodigo = By.xpath("//input[@id='postal-code']");
    By btnContinuar = By.xpath("//input[@id='continue']");
    By btnFinish= By.xpath("//button[@id='finish']");
    By msjCompraRealizada= By.xpath("//span[contains(text(),'Checkout: Complete!')]");
    By btnCancelar = By.xpath("//button[@id='cancel']");
    By btnBackHome = By.xpath("//button[@id='back-to-products']");

    public By getInputNombre() {
        return inputNombre;
    }

    public void setInputNombre(By inputNombre) {
        this.inputNombre = inputNombre;
    }

    public By getInputApellido() {
        return inputApellido;
    }

    public void setInputApellido(By inputApellido) {
        this.inputApellido = inputApellido;
    }

    public By getInputCodigo() {
        return inputCodigo;
    }

    public void setInputCodigo(By inputCodigo) {
        this.inputCodigo = inputCodigo;
    }

    public By getBtnContinuar() {
        return btnContinuar;
    }

    public void setBtnContinuar(By btnContinuar) {
        this.btnContinuar = btnContinuar;
    }

    public By getBtnFinish() {
        return btnFinish;
    }

    public void setBtnFinish(By btnFinish) {
        this.btnFinish = btnFinish;
    }

    public By getMsjCompraRealizada() {
        return msjCompraRealizada;
    }

    public void setMsjCompraRealizada(By msjCompraRealizada) {
        this.msjCompraRealizada = msjCompraRealizada;
    }

    public By getBtnCancelar() {
        return btnCancelar;
    }

    public void setBtnCancelar(By btnCancelar) {
        this.btnCancelar = btnCancelar;
    }

    public By getBtnBackHome() {
        return btnBackHome;
    }

    public void setBtnBackHome(By btnBackHome) {
        this.btnBackHome = btnBackHome;
    }
}
