package com.saucedemo.pageObject;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoguinPageObject extends PageObject {

    By inputUsuario = By.xpath("//input[@id='user-name']");
    By inputContraseña = By.xpath("//input[@id='password']");
    By btnloguin = By.xpath("//input[@id='login-button']");
    By msjNoUser = By.xpath("//div[contains(h3,'Epic sadface: Username is required')]");
    By msjNoPassword = By.xpath("//div[contains(h3,'Epic sadface: Password is required')]");
    By msjNoCoincidePassword = By.xpath("//div[contains(h3,'Epic sadface: Username and password do not match any user in this service')]");
    By msjPantallaProductos = By.xpath("//span[contains(text(),'Products')]");
    By msjBloque = By.xpath("//div[contains(h3,'Epic sadface: Sorry, this user has been locked out.')]");

    public By getInputUsuario() {
        return inputUsuario;
    }

    public void setInputUsuario(By inputUsuario) {
        this.inputUsuario = inputUsuario;
    }

    public By getInputContraseña() {
        return inputContraseña;
    }

    public void setInputContraseña(By inputContraseña) {
        this.inputContraseña = inputContraseña;
    }

    public By getBtnloguin() {
        return btnloguin;
    }

    public void setBtnloguin(By btnloguin) {
        this.btnloguin = btnloguin;
    }

    public By getMsjNoUser() {
        return msjNoUser;
    }

    public void setMsjNoUser(By msjNoUser) {
        this.msjNoUser = msjNoUser;
    }

    public By getMsjNoPassword() {
        return msjNoPassword;
    }

    public void setMsjNoPassword(By msjNoPassword) {
        this.msjNoPassword = msjNoPassword;
    }

    public By getMsjNoCoincidePassword() {
        return msjNoCoincidePassword;
    }

    public void setMsjNoCoincidePassword(By msjNoCoincidePassword) {
        this.msjNoCoincidePassword = msjNoCoincidePassword;
    }

    public By getMsjPantallaProductos() {
        return msjPantallaProductos;
    }

    public void setMsjPantallaProductos(By msjPantallaProductos) {
        this.msjPantallaProductos = msjPantallaProductos;
    }

    public By getMsjBloque() {
        return msjBloque;
    }

    public void setMsjBloque(By msjBloque) {
        this.msjBloque = msjBloque;
    }
}
