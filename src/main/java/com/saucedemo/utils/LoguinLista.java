package com.saucedemo.utils;

public class LoguinLista {

    private String usuario;
    private String contraseña;
    private String url;

    public LoguinLista(String usuario, String contraseña, String url) {
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.url = url;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "LoguinLista{" +
                "url='" + url + '\'' +
                "usuario='" + usuario +
                ", contraseña=" + contraseña +
                '}';
    }

}
