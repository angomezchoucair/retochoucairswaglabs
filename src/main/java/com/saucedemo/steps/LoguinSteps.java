package com.saucedemo.steps;

import com.saucedemo.pageObject.LoguinPageObject;
import com.saucedemo.utils.LoguiExcel;
import com.saucedemo.utils.LoguinLista;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class LoguinSteps {
LoguinPageObject loguinPageObject = new LoguinPageObject();
LoguiExcel datosingre = new LoguiExcel();
List<LoguinLista> loguinListas = datosingre.IngresCredenciales();

@Step
public void abrirNavegador(){
    loguinPageObject.openUrl(loguinListas.get(0).getUrl());
}
@Step
    public void ingresarUsuario(){
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputUsuario()).sendKeys(loguinListas.get(0).getUsuario());
}
@Step
public void ingresarContraseña(){
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputContraseña()).sendKeys(loguinListas.get(0).getContraseña());
}

@Step
    public void clickBotonLoguin(){
    loguinPageObject.getDriver().findElement(loguinPageObject.getBtnloguin()).click();
}
@Step
    public String pantallaProductos(){
    String productos= loguinPageObject.getDriver().findElement(loguinPageObject.getMsjPantallaProductos()).getText();
    return productos;
}

@Step
    public String msjUserError(){
    String user= loguinPageObject.getDriver().findElement(loguinPageObject.getMsjNoUser()).getText();
    return user;
}
@Step
    public String msjPasswordError(){
    String password = loguinPageObject.getDriver().findElement(loguinPageObject.getMsjNoPassword()).getText();
    return password;
}
@Step
    public void usuariosNoValidos(){
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputUsuario()).sendKeys(loguinListas.get(1).getUsuario());
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputContraseña()).sendKeys(loguinListas.get(1).getContraseña());
}
@Step
    public String usuarioNovalido(){
    String noValido = loguinPageObject.getDriver().findElement(loguinPageObject.getMsjNoCoincidePassword()).getText();
    return noValido;
}
@Step
    public void usuarioBloqueado(){
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputUsuario()).sendKeys(loguinListas.get(2).getUsuario());
    loguinPageObject.getDriver().findElement(loguinPageObject.getInputContraseña()).sendKeys(loguinListas.get(2).getContraseña());
}
@Step
    public String msjUsuarioBlock(){
    String bloqueo = loguinPageObject.getDriver().findElement(loguinPageObject.getMsjBloque()).getText();
    return bloqueo;
}


}

