package com.saucedemo.steps;

import com.saucedemo.pageObject.ProductosPageObject;
import net.thucydides.core.annotations.Step;

public class ProductosStesps {
    ProductosPageObject productosPageObject = new ProductosPageObject();

    @Step
    public void clickAñadir(){
        productosPageObject.getDriver().findElement(productosPageObject.getBtnAñadirAlCarrito()).click();
    }
    @Step
    public void clickRemoverDelCarrito(){
        productosPageObject.getDriver().findElement(productosPageObject.getBtnRemoverCarrito()).click();
    }
    @Step
    public void clickImgProducto(){
        productosPageObject.getDriver().findElement(productosPageObject.getImgProducto()).click();
    }
    @Step
    public void clickVolerInicio(){
        productosPageObject.getDriver().findElement(productosPageObject.getBtnVolverInicio()).click();
    }
    @Step
    public void clcikCarrito(){
        productosPageObject.getDriver().findElement(productosPageObject.getBtnCarrito()).click();
    }
    @Step
    public void clickComprar(){
        productosPageObject.getDriver().findElement(productosPageObject.getBtnComprar()).click();
    }

}
