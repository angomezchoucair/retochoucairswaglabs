package com.saucedemo.steps;

import com.saucedemo.pageObject.CompraPageObject;
import com.saucedemo.utils.CompraLista;
import com.saucedemo.utils.LoguiExcel;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class CompraSteps {

    CompraPageObject compraPageObject = new CompraPageObject();
    LoguiExcel datosingre2 = new LoguiExcel();
    List<CompraLista> compraListas1 = datosingre2.IngresCredencialesCompra();
    @Step
    public void llenarInformacion(){
        compraPageObject.getDriver().findElement(compraPageObject.getInputNombre()).sendKeys(compraListas1.get(0).getNombre());
        compraPageObject.getDriver().findElement(compraPageObject.getInputApellido()).sendKeys(compraListas1.get(0).getApellido());
        compraPageObject.getDriver().findElement(compraPageObject.getInputCodigo()).sendKeys(compraListas1.get(0).getCodigo());
    }
    @Step
    public void clcikContinuar(){
    compraPageObject.getDriver().findElement(compraPageObject.getBtnContinuar()).click();
    }

    @Step
    public void clickFinish(){
        compraPageObject.getDriver().findElement(compraPageObject.getBtnFinish()).click();
    }

    @Step
    public String msjFinDecompra(){
        String finCompra = compraPageObject.getDriver().findElement(compraPageObject.getMsjCompraRealizada()).getText();
        return finCompra;
    }
    @Step
    public void llenarInformacionError(){
        compraPageObject.getDriver().findElement(compraPageObject.getInputNombre()).sendKeys(compraListas1.get(1).getNombre());
        compraPageObject.getDriver().findElement(compraPageObject.getInputApellido()).sendKeys(compraListas1.get(1).getApellido());
        compraPageObject.getDriver().findElement(compraPageObject.getInputCodigo()).sendKeys(compraListas1.get(1).getCodigo());
    }
    @Step
    public void clickCancelar(){
        compraPageObject.getDriver().findElement(compraPageObject.getBtnCancelar()).click();
    }
    @Step
    public void clickBackHome(){
        compraPageObject.getDriver().findElement(compraPageObject.getBtnBackHome()).click();
    }

}
