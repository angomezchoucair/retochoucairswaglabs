package com.saucedemo.runner;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/productos.feature",
        glue = "com.saucedemo.stepDefinitions",
        snippets = SnippetType.CAMELCASE)

public class ProductosRunner {
}
