package com.saucedemo.stepDefinitions;

import com.saucedemo.steps.LoguinSteps;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class LoguinStepDefinitions {

    @Steps
    LoguinSteps loguinSteps;

    @Dado("^el usuario digita usuario y contraseña$")
    public void elUsuarioDigitaUsuarioYContraseña() {
    loguinSteps.abrirNavegador();
    loguinSteps.ingresarUsuario();
    loguinSteps.ingresarContraseña();
    }

    @Cuando("^oprime el boton de continuar$")
    public void oprimeElBotonDeContinuar() {
        loguinSteps.clickBotonLoguin();
    }

    @Entonces("^se visualiza el portal de ventas$")
    public void seVisualizaElPortalDeVentas() {
        Assert.assertEquals("PRODUCTS",loguinSteps.pantallaProductos());
    }

    @Dado("^que el usuario solo digita la contraseña$")
    public void queElUsuarioSoloDigitaLaContraseña() {
        loguinSteps.abrirNavegador();
        loguinSteps.ingresarContraseña();
    }

    @Entonces("^Se visualiza el mensaje de error del campo usuario$")
    public void seVisualizaElMensajeDeErrorDelCampoUsuario() {
    Assert.assertEquals("Epic sadface: Username is required",loguinSteps.msjUserError());
    }

    @Dado("^que el usuario solo digita el nombre de usuario$")
    public void queElUsuarioSoloDigitaElNombreDeUsuario() {
    loguinSteps.abrirNavegador();
    loguinSteps.ingresarUsuario();
    }

    @Entonces("^Se visualiza el mensaje de error del campo contraseña$")
    public void seVisualizaElMensajeDeErrorDelCampoContraseña() {
    Assert.assertEquals("Epic sadface: Password is required",loguinSteps.msjPasswordError());
    }

    @Dado("^que el usuario solo digita usuario y contraseña no valida$")
    public void queElUsuarioSoloDigitaUsuarioYContraseñaNoValida() {
        loguinSteps.abrirNavegador();
    loguinSteps.usuariosNoValidos();
    }

    @Entonces("^Se visualiza el mensaje de error de contraseña no coinciden$")
    public void seVisualizaElMensajeDeErrorDeContraseñaNoCoinciden() {
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service",loguinSteps.usuarioNovalido());
    }
    @Dado("^El usuario intenta ingresar con un usuario bloqueado$")
    public void el_usuario_intenta_ingresar_con_un_usuario_bloqueado() {
        loguinSteps.abrirNavegador();
    loguinSteps.usuarioBloqueado();
    }

    @Entonces("^se visualiza el mensaje de usuario bloqueado$")
    public void se_visualiza_el_mensaje_de_usuario_bloqueado() {
    Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.",loguinSteps.msjUsuarioBlock());
    }
}
