package com.saucedemo.stepDefinitions;
import com.saucedemo.steps.CompraSteps;
import com.saucedemo.steps.ProductosStesps;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class ComprarStepDefinitions {
    @Steps
    ProductosStesps productosStesps;
    @Steps
    CompraSteps compraSteps;

    @Cuando("^Oprime el boton de compara$")
    public void oprime_el_boton_de_compara(){
    productosStesps.clickComprar();
    }

    @Cuando("^Llena la infomacion pedida$")
    public void llena_la_infomacion_pedida() {
    compraSteps.llenarInformacion();
    compraSteps.clcikContinuar();
    }

    @Cuando("^Oprime el boton de finish$")
    public void oprime_el_boton_de_finish() {
    compraSteps.clickFinish();
    }

    @Cuando("^se visualiza el mensaje de fin de compra$")
    public void se_visualiza_el_mensaje_de_fin_de_compra() {
    Assert.assertEquals("CHECKOUT: COMPLETE!",compraSteps.msjFinDecompra());
    }
    @Cuando("^oprime el boton de volver al inicio$")
    public void oprime_el_boton_de_volver_al_inicio() {
    compraSteps.clickBackHome();
    }

    @Entonces("^se visualiza el mensaje de error$")
    public void se_visualiza_el_mensaje_de_error() {
        Assert.assertEquals("CHECKOUT: COMPLETE!",compraSteps.msjFinDecompra());
    }

    @Cuando("^Llena la infomacion pedida con datos erroneos$")
    public void llena_la_infomacion_pedida_con_datos_erroneos() {
    compraSteps.llenarInformacionError();
        compraSteps.clcikContinuar();
    }

    @Entonces("^se visualiza el mensaje de error correspondiente$")
    public void se_visualiza_el_mensaje_de_error_correspondiente() {
        Assert.assertEquals("CHECKOUT: COMPLETE!",compraSteps.msjFinDecompra());
    }

    @Cuando("^oprime el boton de cancelar$")
    public void oprime_el_boton_de_cancelar() {
    compraSteps.clickCancelar();
    }
}
