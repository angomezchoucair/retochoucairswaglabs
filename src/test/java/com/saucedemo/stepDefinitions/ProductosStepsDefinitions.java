package com.saucedemo.stepDefinitions;
import com.saucedemo.pageObject.ProductosPageObject;
import com.saucedemo.steps.LoguinSteps;
import com.saucedemo.steps.ProductosStesps;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import org.junit.Assert;

import java.util.regex.Matcher;

public class ProductosStepsDefinitions {

@Steps
    ProductosStesps productosStesps;
@Steps
    ProductosPageObject productosPageObject = new ProductosPageObject();
    @Steps
    LoguinSteps loguinSteps;

    @Dado("^que el usuario agrega al carrito un producto$")
    public void que_el_usuario_agrega_al_carrito_un_producto() {
    productosStesps.clickAñadir();
    }

    @Cuando("^el usuario remueve el producto$")
    public void el_usuario_remueve_el_producto() {
    productosStesps.clickRemoverDelCarrito();
    }

    @Entonces("^se debe visualizar otra vez el boton de añadir$")
    public void se_debe_visualizar_otra_vez_el_boton_de_añadir() {
    Assert.assertThat(productosPageObject.getDriver().findElement(productosPageObject.getBtnAñadirAlCarrito()).isDisplayed(), Matchers.is(true));
    }

    @Dado("^que el usuario da click en un producto$")
    public void que_el_usuario_da_click_en_un_producto() {
    productosStesps.clickImgProducto();
    }

    @Cuando("^Se visualiza el botn de volver a los productos$")
    public void se_visualiza_el_botn_de_volver_a_los_productos() {
    Assert.assertThat(productosPageObject.getDriver().findElement(productosPageObject.getBtnVolverInicio()).isDisplayed(),Matchers.is(true));

    }

    @Entonces("^da click en el boton de volver y se visualiza la pantalla de productos$")
    public void da_click_en_el_boton_de_volver_y_se_visualiza_la_pantalla_de_productos() {
    productosStesps.clickVolerInicio();
        Assert.assertEquals("PRODUCTS",loguinSteps.pantallaProductos());
    }

    @Cuando("^le da click en el carrito$")
    public void le_da_click_en_el_carrito() {
    productosStesps.clcikCarrito();
    }

    @Entonces("^se muestra el producto en el carrito de compras$")
    public void se_muestra_el_producto_en_el_carrito_de_compras() {
    Assert.assertThat(productosPageObject.getDriver().findElement(productosPageObject.getBtnComprar()).isDisplayed(),Matchers.is(true));

    }

}
