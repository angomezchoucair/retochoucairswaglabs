#language:es
  Característica: el usuario compra un item
    Antecedentes:
      Dado el usuario digita usuario y contraseña
      Cuando oprime el boton de continuar
      Entonces se visualiza el portal de ventas

      Escenario: el usuario selecciona un item y lo desea comprar
        Dado que el usuario agrega al carrito un producto
        Cuando le da click en el carrito
        Y Oprime el boton de compara
        Y Llena la infomacion pedida
        Y Oprime el boton de finish
        Y se visualiza el mensaje de fin de compra
        Y oprime el boton de volver al inicio
        Entonces se visualiza el portal de ventas


    Escenario: el usuario no selecciona ningun item y procede quiere realizar el proceso de compra
      Cuando le da click en el carrito
      Y Oprime el boton de compara
      Entonces se visualiza el mensaje de error

    Escenario: El usuario llena los datos pedidos con informacion invalida
      Dado que el usuario agrega al carrito un producto
      Cuando le da click en el carrito
      Y Oprime el boton de compara
      Y Llena la infomacion pedida con datos erroneos
      Entonces se visualiza el mensaje de error correspondiente

      Escenario: El usuario cancela la compra previo a finalizarla
        Dado que el usuario agrega al carrito un producto
        Cuando le da click en el carrito
        Y Oprime el boton de compara
        Y Llena la infomacion pedida
        Y oprime el boton de cancelar
        Entonces se visualiza el portal de ventas
