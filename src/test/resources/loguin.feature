#language:es

  Característica: El usuario ingresa sus credenciales en la pantalla de loguin

    Escenario: El usuario ingresa con credenciales validas

      Dado el usuario digita usuario y contraseña
      Cuando oprime el boton de continuar
      Entonces se visualiza el portal de ventas

    Escenario: El usuario solo ingresa Contraseña
      Dado que el usuario solo digita la contraseña
      Cuando oprime el boton de continuar
      Entonces Se visualiza el mensaje de error del campo usuario

    Escenario: El usuario solo ingresa usuario
      Dado que el usuario solo digita el nombre de usuario
      Cuando oprime el boton de continuar
      Entonces Se visualiza el mensaje de error del campo contraseña

      Escenario: El usuario ingresa una contraseña no valida
        Dado que el usuario solo digita usuario y contraseña no valida
        Cuando oprime el boton de continuar
        Entonces Se visualiza el mensaje de error de contraseña no coinciden

    Escenario: El usuario ingresa con un perfil bloqueado
      Dado El usuario intenta ingresar con un usuario bloqueado
      Cuando oprime el boton de continuar
      Entonces se visualiza el mensaje de usuario bloqueado



