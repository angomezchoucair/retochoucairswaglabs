#language:es
  Característica: El usuario interactua con la primer pantalla

    Antecedentes:
      Dado el usuario digita usuario y contraseña
      Cuando oprime el boton de continuar
      Entonces se visualiza el portal de ventas

    Escenario: El usuario da click en el boton de añadir al carrito y luego lo remueve
        Dado que el usuario agrega al carrito un producto
        Cuando el usuario remueve el producto
        Entonces se debe visualizar otra vez el boton de añadir

    Escenario: El usuario da click en un producto y vuelve a la pantalla de inicio
        Dado que el usuario da click en un producto
        Cuando Se visualiza el botn de volver a los productos
        Entonces da click en el boton de volver y se visualiza la pantalla de productos

    Escenario: El usuario añade un producto al carrtio y se visualiza en el carro
      Dado que el usuario agrega al carrito un producto
      Cuando le da click en el carrito
      Entonces se muestra el producto en el carrito de compras